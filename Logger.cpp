#include "Logger.h"

#include <ctime>
#include <iostream>
#include <Windows.h>
#include <cassert>

std::shared_ptr<ILogger> Log::global_logger;
thread_local std::shared_ptr<ILogger> Log::tls_logger;

std::shared_ptr<ILogger> Log::get_logger() {
	auto thread_local_ = tls_logger;
	if (thread_local_)
		return thread_local_;
	auto global = std::atomic_load(&global_logger);
	assert(global && "No logger is set");
	return global;
}

void Log::set_logger(std::shared_ptr<ILogger> logger) {
	std::atomic_store(&global_logger, logger);
}

void Log::set_thread_local_logger(std::shared_ptr<ILogger> logger) {
	tls_logger = std::move(logger);
}

void Log::error(std::string && msg) {
	get_logger()->log_error(std::move(msg));
}

void Log::warning(std::string && msg) {
	get_logger()->log_warning(std::move(msg));
}

void Log::msg(std::string && msg) {
	get_logger()->log_msg(std::move(msg));
}

void Log::success(std::string && msg) {
	get_logger()->log_success(std::move(msg));
}

void ConsoleLogger::log(const char * prepend, WORD console_attrs, std::string && msg) {
	std::lock_guard<std::mutex> l(mutex);

	auto handle = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO console_info;
	GetConsoleScreenBufferInfo(handle, &console_info);
	unsigned all_foreground_bits = FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY;
	unsigned attrs_mask = ((unsigned)-1 ^ all_foreground_bits) & console_info.wAttributes;

	std::string final_string;
	final_string.reserve(msg.size() + 10 + 80); //msg size + prepend size + datetime size

	time_t rawtime;
	struct tm timeinfo;
	char buffer[80];

	time(&rawtime);
	auto err = localtime_s(&timeinfo, &rawtime);
	if (err) {
		std::cout << "Could not log: " << err << '\n';
		return;
	}

	strftime(buffer, sizeof(buffer), "%H:%M:%S", &timeinfo);


	final_string += '[';
	final_string += buffer;
	final_string += "] ";

	std::cout << final_string;
	final_string.clear();

	final_string += prepend;
	final_string += ": ";
	SetConsoleTextAttribute(handle, attrs_mask | console_attrs);
	std::cout << final_string;
	final_string.clear();
	SetConsoleTextAttribute(handle, console_info.wAttributes);

	final_string += msg;
	std::cout << final_string << '\n';
}

void ConsoleLogger::log_msg(std::string && msg) {
	log("Message", FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE, std::move(msg));
}

void ConsoleLogger::log_success(std::string && msg) {
	log("Success", FOREGROUND_GREEN | FOREGROUND_INTENSITY, std::move(msg));
}

void ConsoleLogger::log_warning(std::string && msg) {
	log("Warning", FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY, std::move(msg));
}

void ConsoleLogger::log_error(std::string && msg) {
	log("  Error", FOREGROUND_RED | FOREGROUND_INTENSITY, std::move(msg));
}
