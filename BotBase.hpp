#pragma once

// To compile this file correctly:
// 1 - Set REMOTE_ADDRESS_X32 or REMOTE_ADDRESS_X64 define depending on what architecture the game is in
// 2 - Add PSAPI_VERSION=1 define
// 3 - Add Psapi.lib to linked in libraries
// 4 - Add shlwapi.lib to linked in libraries
// 5 - Set C++ Language Standard to C++17
// 6 - If you want screenshots define BOTBASE_USE_SCREENSHOT and make sure dx9 is available
// 7 - If you want opencv define BOTBASE_USE_OPENCV and make sure opencv is available


#include <Windows.h>
#include <vector>
#include <ctime>
#include <iostream>
#include <memory>
#include <string>
#include <utility>
#include <regex>
#include <algorithm>
#include <chrono>
#include <random>
#include <thread>
#include <unordered_map>
#include <psapi.h>
#include <shlwapi.h>
#include <string_view>
#include <sstream>
#include <atomic>
#include <charconv>
#include <fstream>
#include <array>
#include <cassert>

#ifdef BOTBASE_USE_SCREENSHOT

#include <Wincodec.h>
#include <d3d9.h> 

#endif

#ifdef BOTBASE_USE_OPENCV

#include <opencv2\opencv.hpp>

#endif

#include "Logger.h"

class BotException {
public:
	const char* user_friendly_message;
	const char* procedure;

	inline explicit BotException(const char* user_friendly_message, const char* procedure)
		: user_friendly_message(user_friendly_message), procedure(procedure) {}
};

class WindowsException {
public:
	const char* user_friendly_message;
	const char* procedure;
	const DWORD windows_error;

	inline explicit WindowsException(const char* user_friendly_message, const char* procedure)
		: user_friendly_message(user_friendly_message), procedure(procedure), windows_error(GetLastError()) {}
};

class BotCancelException {}; //Only the type is required. The bot is stopping.

//Threadsafe
class BotCancellationToken {
	std::shared_ptr<std::atomic<bool>> token = std::make_shared<std::atomic<bool>>();
public:

	//Throws BotCancelException
	inline void throw_if_cancelled() {
		if (is_cancelled())
			throw BotCancelException();
	}

	inline bool is_cancelled() {
		return token->load();
	}

	inline void cancel() {
		token->store(true);
	}
};





inline void read_text_file(std::wstring path, std::string* sink) {
	std::ifstream is;
	is.open(path);
	if (!is.is_open())
		throw BotException("Could not open file", "read_text_file");
	is.seekg(0, std::ios::end);
	auto buffer_length = is.tellg();
	is.seekg(0, std::ios::beg);
	sink->resize(buffer_length);
	is.read(sink->data(), buffer_length);
}





inline bool ends_with(std::wstring_view full, std::wstring_view ending) {
	if (full.length() >= ending.length()) {
		return (0 == full.compare(full.length() - ending.length(), ending.length(), ending));
	}
	else {
		return false;
	}
}

inline void to_hex(unsigned char initialByte, std::string* sink) {
	unsigned char firstNibble = 0U;  // a Nibble is 4 bits, half a byte, one hexadecimal character
	char firstHexChar = 0;
	unsigned char secondNibble = 0U;
	char secondHexChar = 0;


	firstNibble = (initialByte >> 4);  // isolate first 4 bits

	if (firstNibble < 10U)
	{
		firstHexChar = (char)('0' + firstNibble);
	}
	else
	{
		firstNibble -= 10U;
		firstHexChar = (char)('A' + firstNibble);
	}

	secondNibble = (initialByte & 0x0F);  // isolate last 4 bits

	if (secondNibble < 10U)
	{
		secondHexChar = (char)('0' + secondNibble);
	}
	else
	{
		secondNibble -= 10U;
		secondHexChar = (char)('A' + secondNibble);
	}

	*sink += firstHexChar;
	*sink += secondHexChar;
}




namespace BotBaseMath {
	extern const float rad_2_deg;

	inline float sign(float v) {
		if (v >= 0.0f)
			return 1.0f;
		else
			return -1.0f;
	}

	inline float clamp(float v, float min, float max) {
		if (v < min)
			v = 0.0f;
		else if (v > max)
			v = 1.0f;
		return v;
	}

	inline float clamp01(float v) {
		if (v < 0.0f)
			v = 0.0f;
		else if (v > 1.0f)
			v = 1.0f;
		return v;
	}
}

struct Vector3 {
	float x, y, z;

	
	static const float kEpsilon;
	static const float kEpsilonNormalSqrt;

	static const Vector3 zero;
	static const Vector3 one;
	static const Vector3 up;
	static const Vector3 down;
	static const Vector3 left;
	static const Vector3 right;
	static const Vector3 forward;
	static const Vector3 back;
	static const Vector3 positiveInfinity;
	static const Vector3 negativeInfinity;

	inline explicit Vector3() : x(0), y(0), z(0) {}
	inline explicit Vector3(float x, float y, float z) : x(x), y(y), z(z) {}

	inline Vector3 operator+(Vector3 b) {
		Vector3 a(*this);
		return Vector3(a.x + b.x, a.y + b.y, a.z + b.z);
	}

	inline Vector3 operator-(Vector3 b) {
		Vector3 a(*this);
		return Vector3(a.x - b.x, a.y - b.y, a.z - b.z);
	}

	inline Vector3 operator-() {
		Vector3 a(*this);
		return Vector3(-a.x, -a.y, -a.z);
	}

	inline Vector3 operator*(float d) {
		Vector3 a(*this);
		return Vector3(a.x * d, a.y * d, a.z * d);
	}

	inline Vector3 operator/(float d) {
		Vector3 a(*this);
		return Vector3(a.x / d, a.y / d, a.z / d);
	}

	inline bool operator==(Vector3 rhs)
	{
		Vector3 lhs(*this);
		// Returns false in the presence of NaN values.
		return sqr_magnitude(lhs - rhs) < kEpsilon * kEpsilon;
	}

	inline bool operator!=(Vector3 rhs)
	{
		Vector3 lhs(*this);
		// Returns true in the presence of NaN values.
		return !(lhs == rhs);
	}

	inline static Vector3 lerp(Vector3 a, Vector3 b, float t)
	{
		t = BotBaseMath::clamp01(t);
		return Vector3(
			a.x + (b.x - a.x) * t,
			a.y + (b.y - a.y) * t,
			a.z + (b.z - a.z) * t
		);
	}

	inline static Vector3 lerp_unclamped(Vector3 a, Vector3 b, float t)
	{
		return Vector3(
			a.x + (b.x - a.x) * t,
			a.y + (b.y - a.y) * t,
			a.z + (b.z - a.z) * t
		);
	}

	inline static Vector3 move_towards(Vector3 current, Vector3 target, float maxDistanceDelta)
	{
		Vector3 toVector = target - current;
		float dist = toVector.magnitude();
		if (dist <= maxDistanceDelta || dist < std::numeric_limits<float>::epsilon())
			return target;
		return current + toVector / dist * maxDistanceDelta;
	}

	inline static Vector3 SmoothDamp(Vector3 current, Vector3 target, Vector3* currentVelocity, float smoothTime, float maxSpeed, float deltaTime)
	{
#pragma push_macro("max")
#undef max
		smoothTime = std::max(0.0001F, smoothTime);
		float omega = 2.0f / smoothTime;

		float x = omega * deltaTime;
		float exp = 1.0f / (1.0f + x + 0.48f * x * x + 0.235f * x * x * x);
		Vector3 change = current - target;
		Vector3 originalTo = target;

		float maxChange = maxSpeed * smoothTime;
		change = change.clamp_magnitude(change, maxChange);
		target = current - change;

		Vector3 temp = (*currentVelocity + change * omega) * deltaTime;
		*currentVelocity = (*currentVelocity - temp * omega) * exp;
		Vector3 output = target + (change + temp) * exp;

		if (dot(originalTo - current, output - originalTo) > 0)
		{
			output = originalTo;
			*currentVelocity = (output - originalTo) / deltaTime;
		}

		return output;

#pragma pop_macro("max")
	}

	
	inline void set(float x, float y, float z) { this->x = x; this->y = y; this->z = z; }

	inline static Vector3 scale(Vector3 a, Vector3 b) { return Vector3(a.x * b.x, a.y * b.y, a.z * b.z); }

	inline Vector3 scale(Vector3 scale) { x *= scale.x; y *= scale.y; z *= scale.z; return *this; }

	inline static Vector3 cross(Vector3 lhs, Vector3 rhs)
	{
		return Vector3(
			lhs.y * rhs.z - lhs.z * rhs.y,
			lhs.z * rhs.x - lhs.x * rhs.z,
			lhs.x * rhs.y - lhs.y * rhs.x);
	}

	inline static Vector3 reflect(Vector3 inDirection, Vector3 inNormal)
	{
		return inNormal * -2.0f * dot(inNormal, inDirection) + inDirection;
	}

	inline static Vector3 normalize(Vector3 value)
	{
		float mag = magnitude(value);
		if (mag > kEpsilon)
			return value / mag;
		else
			return zero;
	}

	inline Vector3 normalize()
	{
		float mag = magnitude();
		if (mag > kEpsilon)
			*this = *this / mag;
		else
			*this = zero;
		return *this;
	}

	inline static float dot(Vector3 lhs, Vector3 rhs) { 
		return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z; 
	}

	inline static Vector3 project(Vector3 vector, Vector3 onNormal)
	{
		
		float sqrMag = dot(onNormal, onNormal);
		if (sqrMag < std::numeric_limits<float>::epsilon())
			return zero;
		else
			return onNormal * dot(vector, onNormal) / sqrMag;
	}

	inline static Vector3 project_in_plane(Vector3 vector, Vector3 planeNormal)
	{
		return vector - project(vector, planeNormal);
	}

	inline static float angle(Vector3 from, Vector3 to)
	{
		float denominator = std::sqrtf(from.sqr_magnitude() * to.sqr_magnitude());
		if (denominator < kEpsilonNormalSqrt)
			return 0.0f;

		float dot = BotBaseMath::clamp(Vector3::dot(from, to) / denominator, -1.0f, 1.0f);
		return std::acosf(dot) * BotBaseMath::rad_2_deg;
	}

	inline static float signed_angle(Vector3 from, Vector3 to, Vector3 axis)
	{
		float unsignedAngle = angle(from, to);
		float sign = BotBaseMath::sign(dot(axis, cross(from, to)));
		return unsignedAngle * sign;
	}

	inline static float distance(Vector3 a, Vector3 b)
	{
		Vector3 vec = Vector3(a.x - b.x, a.y - b.y, a.z - b.z);
		return std::sqrtf(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z);
	}

	inline static Vector3 clamp_magnitude(Vector3 vector, float maxLength)
	{
		if (vector.sqr_magnitude() > maxLength * maxLength)
			return vector.normalize() * maxLength;
		return vector;
	}

	inline static float magnitude(Vector3 vector) { 
		return std::sqrtf(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z); 
	}

	inline float magnitude() {
		return magnitude(*this);
	}

	inline static float sqr_magnitude(Vector3 vector) { 
		return vector.x * vector.x + vector.y * vector.y + vector.z * vector.z; 
	}

	inline float sqr_magnitude() {
		return sqr_magnitude(*this);
	}

#pragma push_macro("min")
#undef min
	inline static Vector3 min(Vector3 lhs, Vector3 rhs)
	{
		return Vector3(std::min(lhs.x, rhs.x), std::min(lhs.y, rhs.y), std::min(lhs.z, rhs.z));
	}
#pragma pop_macro("min")

#pragma push_macro("max")
#undef max
	inline static Vector3 max(Vector3 lhs, Vector3 rhs)
	{
		return Vector3(std::max(lhs.x, rhs.x), std::max(lhs.y, rhs.y), std::max(lhs.z, rhs.z));
	}
#pragma push_macro("max")

	inline void to_string(std::string* sink) {
		*sink += '(';
		*sink += std::to_string(x);
		*sink += ", ";
		*sink += std::to_string(y);
		*sink += ", ";
		*sink += std::to_string(z);
		*sink += ')';
	}

	inline std::string to_string() {
		std::string buffer;
		to_string(&buffer);
		return buffer;
	}
};

struct Vector2Int {
	int x, y;

	static const Vector2Int zero;
	static const Vector2Int one;
	static const Vector2Int up;
	static const Vector2Int down;
	static const Vector2Int left;
	static const Vector2Int right;

	inline explicit Vector2Int() : x(0), y(0) {}
	inline explicit Vector2Int(int x, int y) : x(x), y(y) {}

	inline Vector2Int operator+(Vector2Int b) {
		Vector2Int a(*this);
		return Vector2Int(a.x + b.x, a.y + b.y);
	}

	inline Vector2Int operator-(Vector2Int b) {
		Vector2Int a(*this);
		return Vector2Int(a.x - b.x, a.y - b.y);
	}

	inline Vector2Int operator-() {
		Vector2Int a(*this);
		return Vector2Int(-a.x, -a.y);
	}

	inline Vector2Int operator*(int d) {
		Vector2Int a(*this);
		return Vector2Int(a.x * d, a.y * d);
	}

	inline Vector2Int operator/(int d) {
		Vector2Int a(*this);
		return Vector2Int(a.x / d, a.y / d);
	}

	inline bool operator==(Vector2Int rhs)
	{
		Vector2Int lhs(*this);
		// Returns false in the presence of NaN values.
		return x == rhs.x && y == rhs.y;
	}

	inline bool operator!=(Vector2Int rhs)
	{
		Vector2Int lhs(*this);
		// Returns true in the presence of NaN values.
		return !(lhs == rhs);
	}

#pragma warning(push)
#pragma warning(disable:4244)
	inline static Vector2Int lerp(Vector2Int a, Vector2Int b, float t)
	{
		t = BotBaseMath::clamp01(t);
		return Vector2Int(
			a.x + (b.x - a.x) * t,
			a.y + (b.y - a.y) * t
		);
	}

	inline static Vector2Int lerp_unclamped(Vector2Int a, Vector2Int b, float t)
	{
		return Vector2Int(
			a.x + (b.x - a.x) * t,
			a.y + (b.y - a.y) * t
		);
	}
#pragma warning(pop)

	inline static Vector2Int move_towards(Vector2Int current, Vector2Int target, int maxDistanceDelta)
	{
		Vector2Int toVector = target - current;
		int dist = toVector.magnitude();
		if (dist <= maxDistanceDelta || dist == 0)
			return target;
		return current + toVector / dist * maxDistanceDelta;
	}

	inline void set(int x, int y) { this->x = x; this->y = y; }

	inline static Vector2Int scale(Vector2Int a, Vector2Int b) { return Vector2Int(a.x * b.x, a.y * b.y); }

	inline Vector2Int scale(Vector2Int scale) { x *= scale.x; y *= scale.y; return *this; }

	inline static int dot(Vector2Int lhs, Vector2Int rhs) {
		return lhs.x * rhs.x + lhs.y * rhs.y;
	}

	inline static Vector2Int project(Vector2Int vector, Vector2Int onNormal)
	{

		int sqrMag = dot(onNormal, onNormal);
		if (sqrMag == 0)
			return zero;
		else
			return onNormal * dot(vector, onNormal) / sqrMag;
	}

#pragma warning(push)
#pragma warning(disable:4244)
	inline static int distance(Vector2Int a, Vector2Int b)
	{
		Vector2Int vec = Vector2Int(a.x - b.x, a.y - b.y);
		return std::sqrt(vec.x * vec.x + vec.y * vec.y);
	}

	inline static int magnitude(Vector2Int vector) {
		return std::sqrt(vector.x * vector.x + vector.y * vector.y);
	}
#pragma warning(pop)

	inline int magnitude() {
		return magnitude(*this);
	}

	inline static int sqr_magnitude(Vector2Int vector) {
		return vector.x * vector.x + vector.y * vector.y;
	}

	inline int sqr_magnitude() {
		return sqr_magnitude(*this);
	}

#pragma push_macro("min")
#undef min
	inline static Vector2Int min(Vector2Int lhs, Vector2Int rhs)
	{
		return Vector2Int(std::min(lhs.x, rhs.x), std::min(lhs.y, rhs.y));
	}
#pragma pop_macro("min")

#pragma push_macro("max")
#undef max
	inline static Vector2Int max(Vector2Int lhs, Vector2Int rhs)
	{
		return Vector2Int(std::max(lhs.x, rhs.x), std::max(lhs.y, rhs.y));
	}
#pragma push_macro("max")

	inline void to_string(std::string* sink) {
		*sink += '(';
		*sink += std::to_string(x);
		*sink += ", ";
		*sink += std::to_string(y);
		*sink += ')';
	}

	inline std::string to_string() {
		std::string buffer;
		to_string(&buffer);
		return buffer;
	}
};

struct Vector2 {
	float x, y;


	static const float kEpsilon;
	static const float kEpsilonNormalSqrt;

	static const Vector2 zero;
	static const Vector2 one;
	static const Vector2 up;
	static const Vector2 down;
	static const Vector2 left;
	static const Vector2 right;
	static const Vector2 positiveInfinity;
	static const Vector2 negativeInfinity;

	inline explicit Vector2() : x(0), y(0) {}
	inline explicit Vector2(float x, float y) : x(x), y(y) {}

	inline Vector2 operator+(Vector2 b) {
		Vector2 a(*this);
		return Vector2(a.x + b.x, a.y + b.y);
	}

	inline Vector2 operator-(Vector2 b) {
		Vector2 a(*this);
		return Vector2(a.x - b.x, a.y - b.y);
	}

	inline Vector2 operator-() {
		Vector2 a(*this);
		return Vector2(-a.x, -a.y);
	}

	inline Vector2 operator*(float d) {
		Vector2 a(*this);
		return Vector2(a.x * d, a.y * d);
	}

	inline Vector2 operator/(float d) {
		Vector2 a(*this);
		return Vector2(a.x / d, a.y / d);
	}

	inline bool operator==(Vector2 rhs)
	{
		Vector2 lhs(*this);
		// Returns false in the presence of NaN values.
		return sqr_magnitude(lhs - rhs) < kEpsilon * kEpsilon;
	}

	inline bool operator!=(Vector2 rhs)
	{
		Vector2 lhs(*this);
		// Returns true in the presence of NaN values.
		return !(lhs == rhs);
	}

	inline static Vector2 lerp(Vector2 a, Vector2 b, float t)
	{
		t = BotBaseMath::clamp01(t);
		return Vector2(
			a.x + (b.x - a.x) * t,
			a.y + (b.y - a.y) * t
		);
	}

	inline static Vector2 lerp_unclamped(Vector2 a, Vector2 b, float t)
	{
		return Vector2(
			a.x + (b.x - a.x) * t,
			a.y + (b.y - a.y) * t
		);
	}

	inline static Vector2 move_towards(Vector2 current, Vector2 target, float maxDistanceDelta)
	{
		Vector2 toVector = target - current;
		float dist = toVector.magnitude();
		if (dist <= maxDistanceDelta || dist < std::numeric_limits<float>::epsilon())
			return target;
		return current + toVector / dist * maxDistanceDelta;
	}

	inline static Vector2 SmoothDamp(Vector2 current, Vector2 target, Vector2* currentVelocity, float smoothTime, float maxSpeed, float deltaTime)
	{
#pragma push_macro("max")
#undef max
		smoothTime = std::max(0.0001F, smoothTime);
		float omega = 2.0f / smoothTime;

		float x = omega * deltaTime;
		float exp = 1.0f / (1.0f + x + 0.48f * x * x + 0.235f * x * x * x);
		Vector2 change = current - target;
		Vector2 originalTo = target;

		float maxChange = maxSpeed * smoothTime;
		change = change.clamp_magnitude(change, maxChange);
		target = current - change;

		Vector2 temp = (*currentVelocity + change * omega) * deltaTime;
		*currentVelocity = (*currentVelocity - temp * omega) * exp;
		Vector2 output = target + (change + temp) * exp;

		if (dot(originalTo - current, output - originalTo) > 0)
		{
			output = originalTo;
			*currentVelocity = (output - originalTo) / deltaTime;
		}

		return output;

#pragma pop_macro("max")
	}


	inline void set(float x, float y) { this->x = x; this->y = y; }

	inline static Vector2 scale(Vector2 a, Vector2 b) { return Vector2(a.x * b.x, a.y * b.y); }

	inline Vector2 scale(Vector2 scale) { x *= scale.x; y *= scale.y; return *this; }

	inline static Vector2 reflect(Vector2 inDirection, Vector2 inNormal)
	{
		return inNormal * -2.0f * dot(inNormal, inDirection) + inDirection;
	}

	inline static Vector2 normalize(Vector2 value)
	{
		float mag = magnitude(value);
		if (mag > kEpsilon)
			return value / mag;
		else
			return zero;
	}

	inline Vector2 normalize()
	{
		float mag = magnitude();
		if (mag > kEpsilon)
			*this = *this / mag;
		else
			*this = zero;
		return *this;
	}

	inline static float dot(Vector2 lhs, Vector2 rhs) {
		return lhs.x * rhs.x + lhs.y * rhs.y;
	}

	inline static Vector2 project(Vector2 vector, Vector2 onNormal)
	{

		float sqrMag = dot(onNormal, onNormal);
		if (sqrMag < std::numeric_limits<float>::epsilon())
			return zero;
		else
			return onNormal * dot(vector, onNormal) / sqrMag;
	}

	inline static Vector2 project_in_plane(Vector2 vector, Vector2 planeNormal)
	{
		return vector - project(vector, planeNormal);
	}

	inline static float angle(Vector2 from, Vector2 to)
	{
		float denominator = std::sqrtf(from.sqr_magnitude() * to.sqr_magnitude());
		if (denominator < kEpsilonNormalSqrt)
			return 0.0f;

		float dot = BotBaseMath::clamp(Vector2::dot(from, to) / denominator, -1.0f, 1.0f);
		return std::acosf(dot) * BotBaseMath::rad_2_deg;
	}

	inline static float signed_angle(Vector2 from, Vector2 to, Vector2 axis)
	{
		float unsignedAngle = angle(from, to);
		float sign = BotBaseMath::sign(from.x * to.y - from.y * to.x);
		return unsignedAngle * sign;
	}

	inline static float distance(Vector2 a, Vector2 b)
	{
		Vector2 vec = Vector2(a.x - b.x, a.y - b.y);
		return std::sqrtf(vec.x * vec.x + vec.y * vec.y);
	}

	inline static Vector2 clamp_magnitude(Vector2 vector, float maxLength)
	{
		if (vector.sqr_magnitude() > maxLength * maxLength)
			return vector.normalize() * maxLength;
		return vector;
	}

	inline static float magnitude(Vector2 vector) {
		return std::sqrtf(vector.x * vector.x + vector.y * vector.y);
	}

	inline float magnitude() {
		return magnitude(*this);
	}

	inline static float sqr_magnitude(Vector2 vector) {
		return vector.x * vector.x + vector.y * vector.y;
	}

	inline float sqr_magnitude() {
		return sqr_magnitude(*this);
	}

#pragma push_macro("min")
#undef min
	inline static Vector2 min(Vector2 lhs, Vector2 rhs)
	{
		return Vector2(std::min(lhs.x, rhs.x), std::min(lhs.y, rhs.y));
	}
#pragma pop_macro("min")

#pragma push_macro("max")
#undef max
	inline static Vector2 max(Vector2 lhs, Vector2 rhs)
	{
		return Vector2(std::max(lhs.x, rhs.x), std::max(lhs.y, rhs.y));
	}
#pragma push_macro("max")

	inline void to_string(std::string* sink) {
		*sink += '(';
		*sink += std::to_string(x);
		*sink += ", ";
		*sink += std::to_string(y);
		*sink += ')';
	}

	inline std::string to_string() {
		std::string buffer;
		to_string(&buffer);
		return buffer;
	}
};






class RemoteAddress {
#ifdef REMOTE_ADDRESS_X64
	static_assert(sizeof(void*) == 8, "REMOTE_ADDRESS_X64 specified on a non 64 bit build");
#define INTERNAL_PTR byte*
#define LITERAL_CONSTRUCTOR_TYPE unsigned long long

#elif REMOTE_ADDRESS_X32
#define INTERNAL_PTR DWORD
#define LITERAL_CONSTRUCTOR_TYPE DWORD

#else
	static_assert(false, "Either the REMOTE_ADDRESS_X64 or the REMOTE_ADDRESS_X32 macro has to be defined to select the size of RemoteAddress class");
#endif


	INTERNAL_PTR ptr;

#ifdef REMOTE_ADDRESS_X64
	static_assert(sizeof(INTERNAL_PTR) == 8, "REMOTE_ADDRESS_X64 is supposed to specify a 64 bit address but it not a 64 bit address.");
#elif REMOTE_ADDRESS_X32
	static_assert(sizeof(INTERNAL_PTR) == 4, "REMOTE_ADDRESS_X32 is supposed to specify a 32 bit address but it is not a 32 address.");
#endif



	
public:
	inline explicit RemoteAddress() : ptr(0) {}
	inline explicit RemoteAddress(void* ptr) : ptr((INTERNAL_PTR) ptr) {}
	inline explicit RemoteAddress(LITERAL_CONSTRUCTOR_TYPE raw) : ptr((INTERNAL_PTR) raw) {}
	RemoteAddress(const RemoteAddress&) = default;
	RemoteAddress(RemoteAddress&&) = default;
	inline void operator=(const RemoteAddress& other) {
		this->ptr = other.ptr;
	};

	inline void* void_ptr() const {
		return (void*) ptr;
	}

	inline INTERNAL_PTR byte_ptr() const {
		return ptr;
	}

	inline bool is_null() const {
		return ptr == 0; //The internal pointer type can be typed as a pointer or as a unsigned number. nullptr does not work in all cases
	}

	inline RemoteAddress operator +(const LITERAL_CONSTRUCTOR_TYPE rhs) const {
		auto ptr = byte_ptr() + rhs;
		return RemoteAddress(ptr);
	}

	inline void operator+=(const LITERAL_CONSTRUCTOR_TYPE rhs) {
		auto ptr = byte_ptr() + rhs;
		this->ptr = ptr;
	}

#undef INTERNAL_PTR
#undef LITERAL_CONSTRUCTOR_TYPE
};

enum class CPUArchitecture {
	x32,
	x64
};

class RemoteProcess {
	HANDLE handle;
	DWORD pid;

	bool try_read_impl(RemoteAddress addr, void* sink, SIZE_T byte_size) {
		auto r = ReadProcessMemory(handle, addr.void_ptr(), sink, byte_size, nullptr);
		return r != 0;
	}

	bool try_write_impl(RemoteAddress addr, void* data, SIZE_T byte_size) {
		auto r = WriteProcessMemory(handle, addr.void_ptr(), data, byte_size, nullptr);
		return r != 0;
	}

public:
	RemoteProcess(const RemoteProcess&&) = delete;
	RemoteProcess(const RemoteProcess&) = delete;
	inline explicit RemoteProcess(DWORD pid)
		: pid(pid) 
	{
		auto handle = ::OpenProcess(PROCESS_ALL_ACCESS, false, pid);
		if (handle == nullptr)
			throw WindowsException("Could not open process", "OpenProcess");
		this->handle = handle;
	}

	inline ~RemoteProcess() {
		CloseHandle(handle);
	}

	inline HANDLE get_handle() {
		return handle;
	}

	inline bool is_running() {
		DWORD exit_code;
		return GetExitCodeProcess(handle, &exit_code) && exit_code == STILL_ACTIVE;
	}

	inline void enforce_running() {
		if (!is_running())
			throw BotException("Bot is not running", "RemoteProcess::enforce_running");
	}

	template<typename T>
	bool try_read(RemoteAddress addr, T* sink) {
		auto r = try_read_impl(addr, sink, sizeof(T));
		return r != 0;
	}

	template<typename T>
	void read(RemoteAddress addr, T* sink) {
		auto r = try_read(addr, sink);
		if (!r)
			throw WindowsException("Could not read process memory", "ReadProcessMemory");
	}

	template<typename T>
	bool try_read_vector(RemoteAddress addr, size_t count, std::vector<T>* sink) {
		sink->resize(count);
		auto buffer_start = sink->data();
		auto buffer_byte_length = sizeof(T) * count;
		return try_read_impl(addr, buffer_start, buffer_byte_length);
	}

	template<typename T>
	void read_vector(RemoteAddress addr, size_t count, std::vector<T>* sink) {
		auto r = try_read_vector(addr, count, sink);
		if (!r)
			throw WindowsException("Could not read process memory", "ReadProcessMemory");
	}

	template<typename T>
	bool try_write(RemoteAddress addr, T& data) {
		auto r = try_write_impl(addr, &data, sizeof(T));
		return r != 0;
	}

	template<typename T>
	void write(RemoteAddress addr, T& data) {
		auto r = try_write(addr, data);
		if (!r)
			throw WindowsException("Could not write process memory", "WriteProcessMemory");
	}

	template<typename T>
	bool try_write_vector(RemoteAddress addr, std::vector<T>& data) {
		auto buffer_start = data.data();
		auto buffer_byte_length = sizeof(T) * data.size();
		return try_write_impl(addr, buffer_start, buffer_byte_length);
	}

	template<typename T>
	void write_vector(RemoteAddress addr, std::vector<T>& data) {
		auto r = try_write_vector(addr, data);
		if (!r)
			throw WindowsException("Could not write process memory", "WriteProcessMemory");
	}

	inline bool try_read_pointers(RemoteAddress base, const int* offsets, RemoteAddress* sink) {
		RemoteAddress next(base);

#pragma push_macro("max")
#undef max
		for (int idx = 0; offsets[idx] != std::numeric_limits<int>::max(); ++idx) {
#pragma pop_macro("max")
			auto r = try_read(next, &next);
			if (!r)
				return false;
			next = RemoteAddress(next.byte_ptr() + offsets[idx]);
		}
		
		*sink = next;
		return true;
	}

	inline void read_pointers(RemoteAddress base, const int* offsets, RemoteAddress* sink) {
		auto r = try_read_pointers(base, offsets, sink);
		if (!r)
			throw BotException("Could not read pointer chain", "Process.read_pointers");
	}

	inline CPUArchitecture architecture() {
		BOOL is_wow_64;

		if (!IsWow64Process(handle, &is_wow_64))
			throw WindowsException("Could not get process architecture", "IsWow64Process");

		if (is_wow_64) {
			return CPUArchitecture::x32;
		}
		else {
			SYSTEM_INFO system_info;
			GetSystemInfo(&system_info);
			switch (system_info.wProcessorArchitecture) {
			case PROCESSOR_ARCHITECTURE_AMD64:
			case PROCESSOR_ARCHITECTURE_ARM64:
			case PROCESSOR_ARCHITECTURE_IA64:
				return CPUArchitecture::x64;
			case PROCESSOR_ARCHITECTURE_INTEL:
				return CPUArchitecture::x32;
			case PROCESSOR_ARCHITECTURE_ARM:
				throw BotException("ARM is not supported", "Process.architecture");
			case PROCESSOR_ARCHITECTURE_UNKNOWN:
				throw BotException("Unknown operating system architecture", "GetSystemInfo");
			default:
				throw BotException("Unhandled enum value SYSTEM_INFO.wProcessArchitecture", "GetSystemInfo");
			}
		}
	}

	inline void memory_scan(
		std::function<void(std::vector<byte> bytes, std::vector<size_t>*)> finder,
		std::vector<RemoteAddress>* sink,
		size_t max_results = 10000) 
	{
		RemoteAddress current;

		std::vector<byte> buffer;
		std::vector<size_t> found_offsets_buffer;

		for (;;) {
			RemoteAddress next_address;
			size_t current_size;

			for (;;) {
				MEMORY_BASIC_INFORMATION memory_info;
				auto r = VirtualQueryEx(handle, current.void_ptr(), &memory_info, sizeof(memory_info));
				if (r == 0)
					throw WindowsException("Could not query memory protection info", "VirtualQueryEx");
				auto memory_chunk_size = memory_info.RegionSize;
				auto protect_attrs = memory_info.Protect;
				next_address = RemoteAddress(current.byte_ptr() + memory_chunk_size);
				current_size = memory_chunk_size;

				switch (protect_attrs) {
				case PAGE_EXECUTE_READ:
				case PAGE_EXECUTE_READWRITE:
				case PAGE_EXECUTE_WRITECOPY:
				case PAGE_READONLY:
				case PAGE_READWRITE:
				case PAGE_WRITECOPY:
					goto read;
				default:
					goto advance;
				}
			}

		read:
			read_vector(current, current_size, &buffer);
			finder(buffer, &found_offsets_buffer);
			for (auto offset : found_offsets_buffer)
				sink->push_back(RemoteAddress(current.byte_ptr() + offset));
			if (sink->size() >= max_results)
				return;

		advance:
			current = next_address;
		}
	}
};

struct ProcessModuleEntry {
	RemoteAddress base;
};

class ProcessModuleInfo {
	std::unordered_map<std::wstring, ProcessModuleEntry> lookup;

public:
	inline explicit ProcessModuleInfo(RemoteProcess& p) {
		update(p);
	}

	void update(RemoteProcess& p) {
		lookup.clear();
		auto handle = p.get_handle();

		DWORD required_bytes;
		auto r = EnumProcessModules(handle, nullptr, 0, &required_bytes);
		if (!r)
			throw WindowsException("Could not get module count in process", "EnumProcessModules");

		auto count = required_bytes / sizeof(HMODULE);
		auto buffer = std::make_unique<HMODULE[]>(count);
		r = EnumProcessModules(handle, buffer.get(), required_bytes, &required_bytes);
		if (!r)
			throw WindowsException("Could not get modules in process", "EnumProcessModules");
		count = required_bytes / sizeof(HMODULE);
		for (unsigned i = 0; i < count; ++i) {
			auto module_handle = buffer[i]; //get required?
			wchar_t name_buffer[MAX_PATH];
			auto name_r = GetModuleFileNameExW(handle, module_handle, name_buffer, sizeof(name_buffer) / sizeof(wchar_t));
			if (!name_r)
				continue;
			PathStripPathW(name_buffer);
			ProcessModuleEntry module_info{ RemoteAddress(module_handle) };
			lookup[std::wstring(name_buffer)] = module_info;
		}
	}

	inline ProcessModuleEntry get(std::wstring name) {
		auto v = lookup.find(name);
		if (v == lookup.end())
			throw BotException("Module not found", "ProcessModuleInfo.get");
		return v->second;
	}
};

inline bool enumerate_processes(std::function<void(DWORD)> cb)
{
	DWORD pid_buffer[4096];
	DWORD bytes_written;

	if (!EnumProcesses(pid_buffer, sizeof(pid_buffer), &bytes_written))
		return false;

	auto pid_count = bytes_written / sizeof(DWORD);

	for (unsigned i = 0; i < pid_count; ++i) {
		auto pid = pid_buffer[i];
		if (pid == 0)
			continue;
		cb(pid);
	}

	return true;
}

inline bool get_process_name(DWORD pid, std::function<void(const wchar_t*)> cb)
{
	wchar_t process_name[MAX_PATH] = L"<unknown>";

	// Get a handle to the process.  
	HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION |
		PROCESS_VM_READ,
		FALSE, pid);

	bool successful = false;
	// Get the process name.  
	if (NULL != hProcess)
	{
		HMODULE hMod;
		DWORD cbNeeded;

		//Given a handle to a process, this returns all the modules running within the process.
		//The first module is the executable running the process,
		//and subsequent handles describe DLLs loaded into the process.
		if (EnumProcessModules(hProcess, &hMod, sizeof(hMod), &cbNeeded))
		{
			//This function returns the short name for a module,
			//typically the file name portion of the EXE or DLL
			GetModuleBaseNameW(hProcess, hMod, process_name,
				sizeof(process_name) / sizeof(wchar_t));

			cb(process_name);
			successful = true;
		}
	}

	//close the process handle
	CloseHandle(hProcess);
	return successful;
}






enum class WindowMessageMethod {
	post,
	send
};

enum class WindowEnforceVisible {
	no,
	exception,
	wait
};

enum WindowVisibilityRequirement {
	on_screen = 0b01,
	focused = 0xb10,
	all = on_screen | focused
};

struct WindowOpOptions {
	std::mt19937* rng_source;
	WindowMessageMethod msg_method = WindowMessageMethod::post;
	WindowEnforceVisible enforce_visible = WindowEnforceVisible::exception;
	WindowVisibilityRequirement visibility_requirements = WindowVisibilityRequirement::all;
	std::chrono::milliseconds base_key_hold_time = std::chrono::milliseconds(100);
	std::chrono::milliseconds diff_key_hold_time = std::chrono::milliseconds(20);
	std::chrono::milliseconds base_mouse_hold_time = std::chrono::milliseconds(100);
	std::chrono::milliseconds diff_mouse_hold_time = std::chrono::milliseconds(20);
	std::chrono::milliseconds wait_between_text_presses = std::chrono::milliseconds(50);
	std::chrono::milliseconds wait_for_window_visibility_frame_length = std::chrono::milliseconds(100);
	std::function<void()> wait_for_window_visibility_one_frame_cb;
};

enum class MouseButton {
	left,
	right,
	middle,
	x1,
	x2
};

enum class MouseModifier {
	none = 0,
	control = 0x8,
	lbutton = 0x1,
	mbutton = 0x10,
	rbutton = 0x2,
	shift = 0x4,
	x1 = 0x20,
	x2 = 0x40
};

class ProcessWindow {
public:
	HWND hwnd;
	WindowOpOptions options;

	inline bool is_focused() {
		auto focused = GetForegroundWindow();
		return focused == hwnd || IsChild(hwnd, focused);
	}

	inline bool try_get_rect(RECT* sink) { //Used by get_rect
		return GetWindowRect(hwnd, sink);
	}

	inline void get_rect(RECT* sink) { //Used by is_visible
		auto r = try_get_rect(sink);
		if (!r)
			throw WindowsException("Could not get window size and position", "GetWindowRect");
	}

	inline bool is_visible() { //Used by private methods
		bool result = true;
		if (options.visibility_requirements & WindowVisibilityRequirement::on_screen) {
			RECT visible_rect;
			auto r = SystemParametersInfoA(SPI_GETWORKAREA, 0, &visible_rect, 0);
			if (!r)
				throw WindowsException("Could not get screen size", "SystemParametersInfoA");
			RECT window_rect;
			get_rect(&window_rect);
			result = window_rect.top >= visible_rect.top
				&& window_rect.right <= visible_rect.right
				&& window_rect.bottom <= visible_rect.bottom
				&& window_rect.left >= visible_rect.left;
		}

		if (result && options.visibility_requirements & WindowVisibilityRequirement::focused) {
			result = is_focused();
		}

		return result;
	}

	inline bool is_valid() {
		return IsWindow(hwnd);
	}

	inline void enforce_valid() {
		if (!is_valid())
			throw BotException("Window is not alive", "enforce_valid");
	}

	inline void enforce_visible() {
		enforce_valid();

		switch (options.enforce_visible) {
		case WindowEnforceVisible::no:
			return; //Visibility is not enforced anyways
		case WindowEnforceVisible::exception:
			if(!is_visible())
				throw BotException("Window is not visible", "enforce_visible");
		case WindowEnforceVisible::wait:
			if (!is_visible()) {
				Log::warning("Bot is paused because window lost focus");
				if (options.wait_for_window_visibility_one_frame_cb)
					options.wait_for_window_visibility_one_frame_cb();
				while (!is_visible()) {
					std::this_thread::sleep_for(options.wait_for_window_visibility_frame_length);
					enforce_valid();
				}
			}
			
			break;
		default:
			throw BotException("Unhandled enum value WindowEnforceVisible", "ProcessWindow.enforce_visible");
		}
	}

private:

	static inline void translate_mouse_buttons(MouseButton in_button, MouseModifier in_mod, bool button_down, UINT* sink_msg, WPARAM* sink_w_param) {

		WPARAM w_param_to_or = static_cast<WPARAM>(MouseModifier::none);
		switch (in_button) {
		case MouseButton::left:
			*sink_msg = WM_LBUTTONDOWN;
			w_param_to_or = MK_LBUTTON;
			break;
		case MouseButton::right:
			*sink_msg = WM_RBUTTONDOWN;
			w_param_to_or = MK_RBUTTON;
			break;
		case MouseButton::middle:
			*sink_msg = WM_MBUTTONDOWN;
			w_param_to_or = MK_MBUTTON;
			break;
		case MouseButton::x1:
			*sink_msg = WM_XBUTTONDOWN;
			w_param_to_or = MK_XBUTTON1;
			break;
		case MouseButton::x2:
			*sink_msg = WM_XBUTTONDOWN;
			w_param_to_or = MK_XBUTTON2;
			break;
		default:
			throw BotException("Unhandled enum value MouseButton", "ProcessWindow.msg_and_wparam");
		}

		if (button_down) {
			*sink_w_param |= w_param_to_or;
		}
		else {
			*sink_msg += 1;
		}
	}

	inline bool deliver(UINT msg, WPARAM w_param, LPARAM l_param, LRESULT send_msg_cmp) {
		enforce_visible();

		switch (options.msg_method) {
		case WindowMessageMethod::send:
			return SendMessageW(hwnd, msg, w_param, l_param) == send_msg_cmp;
		case WindowMessageMethod::post:
			return PostMessageW(hwnd, msg, w_param, l_param) != 0;
		default:
			throw BotException("Unhandled enum value WindowMessageMethod", "ProcessWindow.deliver");
		}
	}

	inline LPARAM mouse_position() {
		POINT point;
		auto r = GetCursorPos(&point);
		if (!r)
			throw WindowsException("Could not get mouse position", "GetCursorPos");
		r = ScreenToClient(hwnd, &point);
		if (!r)
			throw WindowsException("Could not translate screen mouse position to window local position ", "ScreenToClient");
		return MAKELPARAM(point.x, point.y);
	}

	inline  std::chrono::milliseconds calculate_sleep_time(std::chrono::milliseconds base, std::chrono::milliseconds max_delta) {
		auto delta = static_cast<int>(max_delta.count());
		std::uniform_int_distribution<int> distro(-delta, delta);
		auto sleep_time = base;
		sleep_time += std::chrono::milliseconds(distro(*options.rng_source));
		return sleep_time;
	}

public:

	inline explicit ProcessWindow(HWND hwnd, WindowOpOptions options) 
		: hwnd(hwnd), options(options) {}

	inline void set_hwnd(HWND hwnd) {
		this->hwnd = hwnd;
	}

	inline void set_position(Vector2Int pos) {
		RECT rect;
		get_rect(&rect);
		auto width = rect.right - rect.left;
		auto height = rect.bottom - rect.top;
		if (!MoveWindow(hwnd, pos.x, pos.y, width, height, true))
			throw WindowsException("Could not move window", "ProcessWindow::set_position");
	}

	inline bool try_key_down(WPARAM key) {
		return deliver(WM_KEYDOWN, key, 0, 0);
	}

	inline void key_down(WPARAM key) {
		auto r = try_key_down(key);
		if (!r)
			throw BotException("Could not send down key", "ProcessWindow.key_down");
	}

	inline bool try_key_up(WPARAM key) {
		LPARAM l_param = 0;
		l_param |= 1 << 29;
		l_param |= 1 << 30;
		return deliver(WM_KEYUP, key, 1 << 29, 0);
	}

	inline void key_up(WPARAM key) {
		auto r = try_key_up(key);
		if (!r)
			throw BotException("Could not send up key", "ProcessWindow.key_up");
	}

	inline bool try_key_click(WPARAM key) {
		auto sleep_time = calculate_sleep_time(options.base_key_hold_time, options.diff_key_hold_time);
		auto r = try_key_down(key);
		if (!r)
			return false;
		std::this_thread::sleep_for(sleep_time);
		r = try_key_up(key);
		if (!r)
			return false;
		return true;
	}

	inline void key_click(WPARAM key) {
		auto r = try_key_click(key);
		if (!r)
			throw BotException("Could not send key click", "ProcessWindow.key_click");
	}

	inline bool try_mouse_key_down(MouseButton btn, MouseModifier modifiers) {
		UINT msg = 0;
		WPARAM w_param = 0;
		translate_mouse_buttons(btn, modifiers, true, &msg, &w_param);
		return deliver(msg, w_param, mouse_position(), 0);
	}

	inline void mouse_key_down(MouseButton btn, MouseModifier mod) {
		auto r = try_mouse_key_down(btn, mod);
		if (!r)
			throw WindowsException("Could not press mouse button", "ProcessWindow.mouse_key_down");
	}

	inline bool try_mouse_key_up(MouseButton btn, MouseModifier modifiers) {
		UINT msg = 0;
		WPARAM w_param = 0;
		translate_mouse_buttons(btn, modifiers, false, &msg, &w_param);
		return deliver(msg, w_param, mouse_position(), 0);
	}

	inline void mouse_key_up(MouseButton btn, MouseModifier mod) {
		auto r = try_mouse_key_up(btn, mod);
		if (!r)
			throw WindowsException("Could not release mouse button", "ProcessWindow.mouse_key_up");
	}

	inline bool try_mouse_click(MouseButton btn, MouseModifier mod) {
		auto sleep_time = calculate_sleep_time(options.base_mouse_hold_time, options.diff_mouse_hold_time);
		auto r = try_mouse_key_down(btn, mod);
		if (!r)
			return false;
		std::this_thread::sleep_for(sleep_time);
		r = try_mouse_key_up(btn, mod);
		return r;
	}

	inline void mouse_click(MouseButton btn, MouseModifier mod) {
		auto r = try_mouse_click(btn, mod);
		if (!r)
			throw BotException("Could not do mouse click", "ProcessWindow.mouse_click");
	}

	inline void write_text_uni(std::wstring text) {
		for (auto& ch : text) {
			deliver(WM_UNICHAR, ch, 0, 0);
			std::this_thread::sleep_for(options.wait_between_text_presses);
		}
	}

	inline void write_text(std::string text) {
		for (auto& ch : text) {
			deliver(WM_CHAR, ch, 0, 0);
			std::this_thread::sleep_for(options.wait_between_text_presses);
		}
	}
};

enum class WindowMatchingType {
	search,
	match_exact
};

namespace detail {
	struct GetUniqueProcessWindowState {
		DWORD required_pid;
		std::wregex required_regex;
		std::wstring window_class;
		WindowMatchingType matching_type;

		std::wstring text_buffer;
		HWND result;
		int result_count;
		int exception_count;
	};

	inline BOOL CALLBACK enum_unique_process_windows(HWND hwnd, LPARAM l_param) {
		auto state = reinterpret_cast<GetUniqueProcessWindowState*>(l_param);
		try {
			DWORD pid;
			auto thread_id = GetWindowThreadProcessId(hwnd, &pid);
			if (thread_id == 0) //This is an assumption. Docs not actually saying what happens in a failure case
				return TRUE;
			if (pid != state->required_pid)
				return TRUE;

			//Compare text
			auto text_len = GetWindowTextLengthW(hwnd);
			if (text_len == 0)
				return TRUE;
			auto max_buffer_size = text_len + 1; //Because of C string terminator
			state->text_buffer.resize(max_buffer_size);
			auto actual_text_len = GetWindowTextW(hwnd, const_cast<wchar_t*>(state->text_buffer.data()), max_buffer_size);
			if (text_len != actual_text_len)
				return TRUE;
			state->text_buffer.resize(text_len); //Get rid of the zero ending
			switch (state->matching_type) {
			case WindowMatchingType::match_exact:
				if (!std::regex_match(state->text_buffer, state->required_regex))
					return TRUE;
				break;
			case WindowMatchingType::search:
				if (!std::regex_search(state->text_buffer, state->required_regex))
					return TRUE;
				break;
			default:
				throw BotException("Unhandled enum value WindowMatchingType", "enum_unique_process_windows");
			}
			
			//Compare Window Class
			state->text_buffer.clear();
			state->text_buffer.resize(128);
#pragma warning(suppress: 4267)
			actual_text_len = GetClassNameW(hwnd, const_cast<wchar_t*>(state->text_buffer.data()), state->text_buffer.size());
			if (actual_text_len == 0)
				return TRUE;
			state->text_buffer.resize(actual_text_len);

			if (state->text_buffer != state->window_class)
				return TRUE;

			state->result = hwnd;
			++state->result_count;
			return TRUE;
		}
		catch (...) {
			++state->exception_count;
			return FALSE;
		}
	}
}

inline HWND get_unique_process_window_handle(DWORD pid, std::wregex regex, WindowMatchingType matching_type, std::wstring window_class) {
	detail::GetUniqueProcessWindowState state = {};
	state.required_pid = pid;
	state.required_regex = regex;
	state.matching_type = matching_type;
	state.window_class = window_class;

	auto r = EnumWindows(detail::enum_unique_process_windows, reinterpret_cast<LPARAM>(&state));
	if (!r)
		throw WindowsException("Could not get process window", "EnumWindows");
	else if (state.result_count == 0)
		throw BotException("Could not find the window", "EnumWindows");
	else if (state.result_count > 1)
		throw BotException("The process has more than one window with the same caption", "EnumWindows");
	else if (state.exception_count > 0)
		throw BotException("There was an internal error while searching for a unique window", "get_unique_process_window");
	return state.result;
}

inline void enumerate_windows(std::function<void(HWND)> dg) {
	struct State {
		std::function<void(HWND)> dg;
	};
	State state;
	state.dg = dg;

	auto fp = [](HWND hwnd, LPARAM l_param) noexcept -> BOOL {
		State* state = (State*)l_param;
		state->dg(hwnd);
		return true;
	};

	auto r = EnumWindows(fp, (LPARAM)&state);
}

inline std::wstring get_window_name(HWND hwnd) {
	std::wstring result;
	auto text_len = GetWindowTextLengthW(hwnd);
	if (text_len == 0)
		return result;
	WCHAR buffer[2048];
	auto actual_text_len = GetWindowTextW(hwnd, buffer, sizeof(buffer) / sizeof(WCHAR));
	result = std::wstring(buffer, actual_text_len);
	return result;
}

inline std::wstring get_window_class_name(HWND hwnd) {
	std::wstring result;
	WCHAR text_buffer[128];
	auto actual_text_len = GetClassNameW(hwnd, text_buffer, sizeof(text_buffer) / sizeof(WCHAR));
	if (actual_text_len == 0)
		return result;
	result = std::wstring(text_buffer, actual_text_len);
	return result;
}

inline Vector2Int relative_pixel_position(ProcessWindow& w, int x, int y, int expected_size_x, int expected_size_y) {
	RECT r;
	w.get_rect(&r);
	auto width = r.right - r.left;
	auto height = r.bottom - r.top;
	if (width != expected_size_x
		|| height != expected_size_y)
		throw BotException("Window is not of the right dimensions.", "relative_pixel_position");
	return Vector2Int(x, y);
}





namespace detail {
	struct DeleterBase { //Only meant for zero size optimization (Works for sub-classes)

	};
#ifdef BOTBASE_USE_SCREENSHOT
	struct Direct3D9Deleter : public DeleterBase {
		void operator() (IDirect3D9* d) {
			d->Release();
		}
	};

	struct Direct3D9DeviceDeleter : public DeleterBase {
		void operator() (IDirect3DDevice9* d) {
			d->Release();
		}
	};

	struct Direct3DSurfaceDeleter : public DeleterBase {
		void operator() (IDirect3DSurface9* d) {
			d->Release();
		}
	};
#endif
}

#ifdef BOTBASE_USE_SCREENSHOT

struct ScreenShotResourceData {
	using D3D = std::unique_ptr<IDirect3D9, detail::Direct3D9Deleter>;
	using Device = std::unique_ptr<IDirect3DDevice9, detail::Direct3D9DeviceDeleter>;
	using Surface = std::unique_ptr<IDirect3DSurface9, detail::Direct3DSurfaceDeleter>;

	D3D d3d;
	Device device;
	Surface surface;
	D3DDISPLAYMODE mode;
	UINT pitch;
	D3DLOCKED_RECT rc;
	std::unique_ptr<BYTE[]> shot;

	static const UINT ADAPTER = D3DADAPTER_DEFAULT;
};

#define WIDEN2(x) L ## x
#define WIDEN(x) WIDEN2(x)
#define __WFILE__ WIDEN(__FILE__)
#define HRCHECK(__expr) {hr=(__expr);if(FAILED(hr)){throw BotException("DirectX error. Expression: \n" "__expr", "ScreenShotResource");}}
#define RELEASE(__p) {if(__p!=nullptr){__p->Release();__p=nullptr;}}

inline ScreenShotResourceData create_screenshot_resource_data() {
	ScreenShotResourceData data;

	auto adapter = ScreenShotResourceData::ADAPTER;
	HRESULT hr = S_OK;
	D3DPRESENT_PARAMETERS parameters = { 0 };

	// init D3D and get screen size
	data.d3d = ScreenShotResourceData::D3D(Direct3DCreate9(D3D_SDK_VERSION));
	HRCHECK(data.d3d->GetAdapterDisplayMode(adapter, &data.mode));

	parameters.Windowed = TRUE;
	parameters.BackBufferCount = 1;
	parameters.BackBufferHeight = data.mode.Height;
	parameters.BackBufferWidth = data.mode.Width;
	parameters.SwapEffect = D3DSWAPEFFECT_DISCARD;
	parameters.hDeviceWindow = NULL;

	// create device & capture surface
	IDirect3DDevice9* device;
	HRCHECK(data.d3d->CreateDevice(adapter, D3DDEVTYPE_HAL, NULL, D3DCREATE_SOFTWARE_VERTEXPROCESSING, &parameters, &device));
	data.device = ScreenShotResourceData::Device(device);

	IDirect3DSurface9* surface;
	HRCHECK(data.device->CreateOffscreenPlainSurface(data.mode.Width, data.mode.Height, D3DFMT_A8R8G8B8, D3DPOOL_SYSTEMMEM, &surface, nullptr));
	data.surface = ScreenShotResourceData::Surface(surface);

	// compute the required buffer size
	HRCHECK(data.surface->LockRect(&data.rc, NULL, 0));
	data.pitch = data.rc.Pitch;
	HRCHECK(data.surface->UnlockRect());

	// allocate screenshots buffer
	data.shot = std::make_unique<BYTE[]>(data.pitch * data.mode.Height);

	return data;
}

class ScreenShotResource {
	ScreenShotResourceData data;

public:

	inline ScreenShotResource() 
		: data(create_screenshot_resource_data()) {}

	inline void take_screenshot() {		

		HRESULT hr = S_OK;

		// get the data
		HRCHECK(data.device->GetFrontBufferData(0, data.surface.get()));

		// copy it into our buffers
		HRCHECK(data.surface->LockRect(&data.rc, NULL, 0));
		CopyMemory(data.shot.get(), data.rc.pBits, data.rc.Pitch * data.mode.Height);
		HRCHECK(data.surface->UnlockRect());
	}

	inline BYTE* current_shot() const {
		return data.shot.get();
	}

	inline D3DDISPLAYMODE mode() const {
		return data.mode;
	}

	inline UINT pitch() const {
		return data.pitch;
	}

	inline cv::Mat cv_mat() const {
		auto mode = this->mode();
		return cv::Mat(mode.Height, mode.Width, CV_8UC4, current_shot());
	}
};

class Image {
	UINT _width, _height, _pitch;
	std::unique_ptr<BYTE[]> _bytes;
public:

	inline Image(const ScreenShotResource& screen_shot, RECT part) {
		auto mode = screen_shot.mode();

		auto part_width = part.right - part.left;
		auto part_height = part.bottom - part.top;
		auto part_pitch = part_width * 4;

#pragma warning(push)
#pragma warning(disable:4018)
		if (part.left >= part.right
			|| part.top >= part.bottom
			|| part.right > mode.Width
			|| part.bottom > mode.Height
			|| part.left < 0
			|| part.top < 0
			)
			throw BotException("Part is out of bounds", "Image::Image");
#pragma warning(pop)

		auto screen_shot_bytes = screen_shot.current_shot();
		auto part_bytes = std::make_unique<BYTE[]>(part_pitch * part_height);
		for (size_t y = part.top; y < part.bottom; ++y) {
			for (size_t x = part.left; x < part.right; ++x) {
				auto source_index = y * mode.Width * 4 + x * 4;
				auto b = screen_shot_bytes[source_index];
				auto g = screen_shot_bytes[source_index + 1];
				auto r = screen_shot_bytes[source_index + 2];
				auto a = screen_shot_bytes[source_index + 3];

				auto target_y = y - part.top;
				auto target_x = x - part.left;
				auto target_index = target_y * part_width * 4 + target_x * 4;
				part_bytes[target_index + 3] = a;
				part_bytes[target_index + 2] = r;
				part_bytes[target_index + 1] = g;
				part_bytes[target_index] = b;
			}
		}

		this->_width = part_width;
		this->_height = part_height;
		this->_pitch = part_pitch;
		this->_bytes = std::move(part_bytes);
	}

	inline UINT width() {
		return _width;
	}

	inline UINT height() {
		return _height;
	}

	inline UINT pitch() {
		return _pitch;
	}

	inline BYTE* bytes() {
		return _bytes.get();
	}

	//cv::Mat is not owning the data here. 
	inline cv::Mat cv_mat() {
		return cv::Mat(height(), width(), CV_8UC4, bytes());
	}
};

//NOTICE: This is a redefinition and it is meant for that. The below function is C style.
#pragma warning(push)
#pragma warning(disable:4005)
#define HRCHECK(__expr) {hr=(__expr);if(FAILED(hr)){wprintf(L"FAILURE 0x%08X (%i)\n\tline: %u file: '%s'\n\texpr: '" WIDEN(#__expr) L"'\n",hr, hr, __LINE__,__WFILE__);goto cleanup;}}
#pragma warning(pop)

inline HRESULT SavePixelsToFile32bppPBGRA(UINT width, UINT height, UINT stride, LPBYTE pixels, LPWSTR filePath, const GUID &format)
{
	if (!filePath || !pixels)
		return E_INVALIDARG;

	HRESULT hr = S_OK;
	IWICImagingFactory *factory = nullptr;
	IWICBitmapEncoder *encoder = nullptr;
	IWICBitmapFrameEncode *frame = nullptr;
	IWICStream *stream = nullptr;
	GUID pf = GUID_WICPixelFormat32bppPBGRA;
	BOOL coInit = CoInitialize(nullptr);

	HRCHECK(CoCreateInstance(CLSID_WICImagingFactory, nullptr, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&factory)));
	HRCHECK(factory->CreateStream(&stream));
	HRCHECK(stream->InitializeFromFilename(filePath, GENERIC_WRITE));
	HRCHECK(factory->CreateEncoder(format, nullptr, &encoder));
	HRCHECK(encoder->Initialize(stream, WICBitmapEncoderNoCache));
	HRCHECK(encoder->CreateNewFrame(&frame, nullptr)); // we don't use options here
	HRCHECK(frame->Initialize(nullptr)); // we dont' use any options here
	HRCHECK(frame->SetSize(width, height));
	HRCHECK(frame->SetPixelFormat(&pf));
	HRCHECK(frame->WritePixels(height, stride, stride * height, pixels));
	HRCHECK(frame->Commit());
	HRCHECK(encoder->Commit());

cleanup:
	RELEASE(stream);
	RELEASE(frame);
	RELEASE(encoder);
	RELEASE(factory);
	if (coInit) CoUninitialize();
	return hr;
}

#undef WIDEN2
#undef WIDEN
#undef __WFILE__
#undef HRCHECK
#undef RELEASE

#endif


class KeyValueSAXVisitor {
public:
	virtual void on_key(std::string_view k, std::string_view v) = 0;
};

class KeyValueSAXConsolePrinter : public KeyValueSAXVisitor {
public:
	inline void on_key(std::string_view k, std::string_view v) override {
		std::cout << k << ": " << v << '\n';
	}
};

inline void parse_key_value_sax(std::string& str, KeyValueSAXVisitor* visitor) {
	std::istringstream f(str);
	std::string line;
	while (std::getline(f, line)) {
		if (line.empty())
			continue;
		auto kv_delim = line.find(':');
		if (kv_delim == -1)
			continue;
		std::string_view k(line.c_str(), kv_delim);
		auto value_len = line.size() - kv_delim - 1;
		//TODO: Proper value_len check
		for (size_t i = 0; value_len > 0; ++i) {
			if (!isspace(line[kv_delim + 1 + i]))
				break;
			++kv_delim;
			--value_len;
		}
		std::string_view v(&line[kv_delim] + 1, value_len);
		visitor->on_key(k, v);
	}
}

class RectInfoVisitor : public KeyValueSAXVisitor {
	std::unordered_map<std::string, RECT> data;

	static void check_comma(size_t it, size_t end) {
		if (it == end)
			throw BotException("Rect Info entry does not have the format TRBL(0,0,0,0)", "RectInfoVisitor::on_key");
	}

	static void parse_long(std::string_view str, size_t first, size_t last, LONG* sink) {
		auto res = std::from_chars(&str[first], &str[last], *sink);
		if (res.ec == std::errc::invalid_argument)
			throw BotException("Rect Info entry does not have the format TRBL(0,0,0,0)", "RectInfoVisitor::on_key");
	}

public:
	inline void on_key(std::string_view k, std::string_view v) override {
		if (!v._Starts_with("TRBL(")) {
			throw BotException("Rect Info entry does not start with TRBL(", "RectInfoVisitor::on_key");
		}
		if (v[v.size() - 1] != ')')
			throw BotException("Rect Info entry does not end with '('", "RectInfoVisitor::on_key");
		size_t c0 = 5;
		auto c1 = v.find(',', 6);
		check_comma(c1, v.length());
		auto c2 = v.find(',', c1 + 1);
		check_comma(c2, v.length());
		auto c3 = v.find(',', c2 + 1);
		check_comma(c3, v.length());
		RECT entry;
		parse_long(v, c0, c1, &entry.top);
		parse_long(v, c1 + 1, c2, &entry.right);
		parse_long(v, c2 + 1, c3, &entry.bottom);
		parse_long(v, c3 + 1, v.length() - 1, &entry.left);

		data[std::string(k)] = entry;
	}

	inline std::unordered_map<std::string, RECT> consume_data() {
		return std::move(data);
	}
};

inline std::unordered_map<std::string, RECT> parse_rect_info(std::string& text) {
	RectInfoVisitor visitor;
	parse_key_value_sax(text, &visitor);
	return visitor.consume_data();
}





struct BotBaseUUID {
	using Array = std::array<uint8_t, 16>;

	Array bytes;

	inline BotBaseUUID() {
		bytes.fill(0);
	}

	inline BotBaseUUID(
		uint8_t a,
		uint8_t b,
		uint8_t c,
		uint8_t d,
		uint8_t e,
		uint8_t f,
		uint8_t g,
		uint8_t h,
		uint8_t i,
		uint8_t j,
		uint8_t k,
		uint8_t l, 
		uint8_t m, 
		uint8_t n, 
		uint8_t o, 
		uint8_t p)
	{
		bytes[0] = a;
		bytes[1] = b;
		bytes[2] = c;
		bytes[3] = d;
		bytes[4] = e;
		bytes[5] = f;
		bytes[6] = g;
		bytes[6] = h;
		bytes[8] = i;
		bytes[9] = j;
		bytes[10] = k;
		bytes[11] = l;
		bytes[12] = m;
		bytes[13] = n;
		bytes[14] = o;
		bytes[15] = p;
	}

	inline bool operator==(const BotBaseUUID& other) const {
		return bytes == other.bytes;
	}

	inline bool is_zeros() const {
		const Array zeros = BotBaseUUID().bytes;
		return this->bytes == zeros;
	}

	inline void zero_out() {
		bytes.fill(0);
	}

	inline std::string to_string() const {
		std::string r;
		r.reserve(36);

		to_hex(bytes[0], &r);
		to_hex(bytes[1], &r);
		to_hex(bytes[2], &r);
		to_hex(bytes[3], &r);
		r += '-';
		to_hex(bytes[4], &r);
		to_hex(bytes[5], &r);
		r += '-';
		to_hex(bytes[6], &r);
		to_hex(bytes[7], &r);
		r += '-';
		to_hex(bytes[8], &r);
		to_hex(bytes[9], &r);
		r += '-';
		to_hex(bytes[10], &r);
		to_hex(bytes[11], &r);
		to_hex(bytes[12], &r);
		to_hex(bytes[13], &r);
		to_hex(bytes[14], &r);
		to_hex(bytes[15], &r);

		return r;
	}
};


struct LoadResourceResult {
	LPVOID ptr;
	DWORD size;
};

inline LoadResourceResult load_resource(HMODULE image_handle, int id, const char* type) {
	auto res = FindResourceA(image_handle, MAKEINTRESOURCEA(id), type);
	if (!res) {
		throw WindowsException("Could not find resource", "load_resource");
	}

	auto handle = LoadResource(image_handle, res);
	if (!handle)
		throw WindowsException("Could not load resource", "load_resource");

	auto ptr = LockResource(handle);
	if (ptr == nullptr)
		throw WindowsException("Could not lock resource", "load_resource");

	auto size = SizeofResource(image_handle, res);
	if (!size)
		throw WindowsException("Could not get resource size", "load_resource");
	return LoadResourceResult{ ptr, size };
}

inline void load_text_resource(HMODULE image_handle, int id, std::string* sink) {
	auto res = load_resource(image_handle, id, "TEXT");
	*sink = std::string((char*)res.ptr, res.size);
}

#ifdef BOTBASE_USE_OPENCV

inline void load_png_resource(HMODULE image_handle, int id, cv::Mat* sink, int decode_flags = cv::IMREAD_COLOR) {
	auto res = load_resource(image_handle, id, "PNG");

	cv::Mat raw_data(1, res.size, CV_8UC1, res.ptr);
	*sink = cv::imdecode(raw_data, cv::IMREAD_COLOR);
	if (sink->data == nullptr)
		throw WindowsException("Could not decode a bitmap resource", "load_bitmap_resource");
}

#endif