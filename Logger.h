#pragma once

#include <string>
#include <memory>
#include <Windows.h>
#include <mutex>

class ILogger {
public:
	virtual ~ILogger() {};
	virtual void log_msg(std::string&& msg) = 0;
	virtual void log_success(std::string&& msg) = 0;
	virtual void log_warning(std::string&& msg) = 0;
	virtual void log_error(std::string&& msg) = 0;
};

class Log {
	static thread_local std::shared_ptr<ILogger> tls_logger;
	static std::shared_ptr<ILogger> global_logger;

	static std::shared_ptr<ILogger> get_logger();

public:
	static void set_logger(std::shared_ptr<ILogger> logger);
	static void set_thread_local_logger(std::shared_ptr<ILogger> logger);
	static void error(std::string&& msg);
	static void warning(std::string&& msg);
	static void msg(std::string&& msg);
	static void success(std::string&& msg);
};

class ConsoleLogger : public ILogger {
private:
	std::mutex mutex;

	void log(const char* prepend, WORD console_attrs, std::string&& msg);

public:
	void log_msg(std::string&& msg) override;
	void log_success(std::string&& msg) override;
	void log_warning(std::string&& msg) override;
	void log_error(std::string&& msg) override;
};